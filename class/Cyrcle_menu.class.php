<?php

	class Cyrcle_menu
	{
		public $options;
		function __construct()
		{
			$this->get_options();
			add_filter("eb_circle_menu_elements", array($this, "elems"));
		}
		function draw()
		{
			global $Soling_Metagame_Constructor;
			$id			= get_current_user_id();
			$user		= get_userdata($id);
			$avatar		= get_avatar( $id , 70, "", $user->display_name, array('extra_attr'=>"style='width:100px!important;'"));
			$buttons	= apply_filters("eb_circle_menu_elements", array());
			//$buttons	= array_merge($buttons, array_fill (0, 6, array("hint"=>"111", "picto"=>SMC_URLPATH . "icon/logout_ico.png")));
			$step		= 45;
			$dl			= 67;
			$dl2		= 102;
			$i = 0;
			$eb_button_hidden = get_option("eb_button_hidden");
			if(!is_array($eb_button_hidden)) $eb_button_hidden= array();
			foreach($buttons as $button)
			{
				if($eb_button_hidden[$button['slug']]) continue;
				$x		= $i>5 && $i<11 ? sin(deg2rad($step * ($i-6) + 311))* $dl2 + 40 : sin(deg2rad($step * $i + 290))* $dl + 43 ;
				$y		= $i>5 && $i<11 ? cos(deg2rad($step * ($i-6) + 311))* $dl2 + 40 : cos(deg2rad($step * $i + 290))* $dl + 43;
				$class	= $i>5 && $i<11 ? "eb_cbutton2 hint--bottom" : "eb_cbutton hint--top";
				$img	= $button['picto'] ? "<img src='".$button['picto']."'>" : "";
				$eb_cir	.= "<div class='$class hint' data-hint='" . $button['hint']."'style='z-index:".(10-$i)."; top:" . $y . "px; left:" . $x . "px' exec='".$button['exec']."'>$img</div>";
				$i++;
			}
			$left		= $this->options['left'];
			$top		= $this->options['top'];
			$html		= "
			<div id='eb_cyrcle_menu' top='$top' left='$left'style='top:".$top."px; left:". $left."px; ".$Soling_Metagame_Constructor->get_klapan_bg()."' class=''>
				<div id='eb_cm_shtrich'  class='lp-border-color'>
				</div>
				<div id='avacenter' class='lp-widget-avatar' style='position:absolute; top:30px; left:30px;'>	
					$avatar
				</div>".
				$eb_cir.
			"</div>";		
			return $html;
		}
		function get_options()
		{
			global $smc_height;
			$this->options		= get_user_meta(get_current_user_id(), ERMAK_BOOKER);
			$this->options		= $this->options[0];
			if(!is_array($this->options))
				$this->options	= array();
			if(!$this->options['top'])
			{
				$this->options['top'] = $smc_height - 200;
				$this->update_options();
			}
			if(!$this->options['left'])
			{
				$this->options['left'] = $smc_height+100;
				$this->update_options();
			}
			return $this->options;
		}
		function update_options()
		{
			update_user_meta(get_current_user_id(), ERMAK_BOOKER, $this->options);
		}
		function elems($arr)
		{
			$arr[]		= array("picto"=> SMP_URLPATH."icon/product_ico.png", 	"hint"=> __("all Factories", "smp"),	"slug"=>"production",		"exec" => "show_factories");
			$arr[]		= array("picto"=> SMP_URLPATH."icon/logistics_ico.png", "hint"=> __("all Hubs", "smp"), 		"slug"=>"logistics",		"exec" => "show_hubs");
			$arr[]		= array("picto"=> SMP_URLPATH."icon/goods_ico.png", 	"hint"=> __("all Goods", "smp"),		"slug"=>"goods",			"exec" => "show_mine_gt");
			$arr[]		= array("picto"=> SMP_URLPATH."icon/money_ico.png", 	"hint"=> __("Bank", "smp"),				"slug"=>"money",			'exec' => 'eb_show_bank');
			$arr[]		= array("picto"=> SMP_URLPATH."icon/calc_ico.png", 		"hint"=> __("Personal Tools", "smp"),	"slug"=>"calc",				'exec' => "eb_show_tools");
			$arr[]		= array("picto"=> SMC_URLPATH."icon/location_ico.png", 	"hint"=> __("Locations", "smc"), 		"slug"=>"location",			'exec' => 'eb_show_locations');
			//$arr[]		= array("picto"=> SMC_URLPATH."icon/talk_ico.png", 		"hint"=> __("all Direct Messages", "smc"));
			//$arr[]		= array("picto"=> "", "hint"=> __("", "smc"));
			return $arr;
		}
	}
?>