<?php
/*
Plugin Name: Ermak.Booker
Plugin URI: http://metaversitet.tv/?page_id=1496_dop
Description: .
Version: 1.0.0
Date: 28.08.2015
Author: Genagl
Author URI: http://www.genagl.ru/author
License: GPL2
*/
/*  Copyright 2015  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/ 

//библиотека переводов
function init_textdomain_ermak_booker() 
{ 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("ermak_booker", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
}
add_action('plugins_loaded', 'init_textdomain_ermak_booker');

function eb_control_plugins()
{
	global $eb_sticker;
	$arr		= array();
	if(!is_plugin_active('Ermak/Ermak.php'))
	{
		$arr[]	= __('Ermak. Metagame', ERMAK_BOOKER);
	}
	if(!is_plugin_active('Ermak_production/Ermak_production.php'))
	{
		$arr[]	= __('Ermak Production', ERMAK_BOOKER);
	}
	if(!is_plugin_active('Ermak_passport/Ermak_passport.php'))
	{
		$arr[]	= __('Ermak Passport', ERMAK_BOOKER);
	}
	if(count($arr)==0)	return true;
	$eb_sticker		= '<div><board_title>'.__("critical gaps plugin functionality Ermak Booker", ERMAK_BOOKER)."</board_title>";
	$eb_sticker		.= __('Please install the following plugins', ERMAK_BOOKER).'<ul>';
	foreach($arr as $a)
	{
		$eb_sticker		.= "<li>".$a."</li>";
	}
	$eb_sticker		.= "</ul>".__("Sorry. Ermak Booker not working", ERMAK_BOOKER)."</div>";
	add_action( 'admin_notices',					'after_install_sticker_eb' , 23);
	return false;
}
function after_install_sticker_eb()
{
	global $eb_sticker;
	echo "
	<div class='updated notice is-dismissible1' id='install_eb_notice' style='padding:30px!important; position: relative;'>
		".
			$eb_sticker .
		"
		<span class='smc_desmiss_button'>
			<span class='screen-reader-text'>".__("Close")."</span>
		</span>
	</div>
	
	";
}
if(!eb_control_plugins())	return;

//Paths
define('ERMAK_BOOKER_URLPATH', WP_PLUGIN_URL.'/Ermak_Booker/');
define('ERMAK_BOOKER_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('ERMAK_BOOKER_PLUGIN', 'Ermak_Booker_Plugin');
define('ERMAK_BOOKER', 'ermak_booker');
define('TOP_MENU_TYPE', 'top_menu');
define('CIRCLE_MENU_TYPE', 'cyrcle_menu');
define('EB_PERSONAL_MOBILE_PAGE', "eb_player_slide");
define('EB_GOODS_BATCHS_MOBILE_PAGE', "eb_gb_slide");
define('EB_STORE_MOBILE_PAGE', "eb_store_slide");
define('EB_PRODUCTS_MOBILE_PAGE', "eb_production_slide");
define('EB_ADMINISTRATOR_PAGE', "eb_admin_slide");
define('EB_BANK_MOBILE_PAGE', "eb_bank_slide");

require_once(ERMAK_BOOKER_REAL_PATH."class/Ermak_Booker_Plugin.php");
require_once(ERMAK_BOOKER_REAL_PATH."class/EB_Personal.class.php");
require_once(ERMAK_BOOKER_REAL_PATH."class/Cyrcle_menu.class.php");
require_once(ERMAK_BOOKER_REAL_PATH."widget/EB_Float_Widget.php");
register_activation_hook( __FILE__, array( Ermak_Booker_Plugin, 'activate' ) );
if (function_exists('register_deactivation_hook'))
{
	register_deactivation_hook(__FILE__, array(Ermak_Booker_Plugin, 'deactivate'));
}
/**/

$Ermak_Miph				= Ermak_Booker_Plugin::get_instance();
$EB_Personal			= new EB_Personal;