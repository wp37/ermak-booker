<?php
	function personal_smartphone()
	{
		global $smc_height;
		$instance		= Ermak_Booker_Plugin::get_instance();
		$options		= $instance->options;
		$smc_height 	= 900;
		$smc_width		= 450;
		$w				= 40;
		$slides			= "";
		if(!is_array($options['mobile_slides'])) $options['mobile_slides'] = array();
		$i				= false;
		foreach($options['mobile_slides'] as $mobile_slides)
		{
			list($slide, $data)	= refresh_slide($mobile_slides, $i);
			$slides		.= $slide;
			$i			= true;
		}
		list($slide, $data)	= refresh_slide(EB_ADMINISTRATOR_PAGE, $i);
		$slides			.= $slide;
		$html	= '
		<div style="width:100%;height:100%;padding:0px;" id="jssor_parent">
			<div class="container" id="mfcc">
				<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: ' . ($smc_width-$w) . 'px; height: '. $smc_height . 'px; overflow: hidden;">'.
					refresh_slide(EB_PERSONAL_MOBILE_PAGE).
					apply_filters("ermak_booker_mobile_slide_1", "").
					refresh_slide(EB_BANK_MOBILE_PAGE, true).
					apply_filters("ermak_booker_mobile_slide_2", "").
					refresh_slide(EB_GOODS_BATCHS_MOBILE_PAGE, true).
					refresh_slide(EB_STORE_MOBILE_PAGE, true).
					refresh_slide(EB_PRODUCTS_MOBILE_PAGE, true).
					apply_filters("ermak_booker_mobile_slide_3", "").
					refresh_slide(EB_ADMINISTRATOR_PAGE, true)./**/
					$slides.
				'</div>
				
				 <!-- navigator container -->
				<div u="navigator" class="navi_buttons" 
				style="	position: absolute;					
						bottom: -25px;
						left: 10px;
						padding: 30px 30px 60px;
						
						height: 30px;">
						<div style="
							position: absolute;
							top: -10px;
							left: -10px;
							display: inline-block;
							background: rgb(0, 0, 0);
							width: 400px;
							height: 100px;
							border: 0;
							-webkit-border-radius: 1px;
							border-radius: 1px;
						">
							
						</div>
					<!-- navigator item prototype -->
					<div u="prototype" style="POSITION: absolute; WIDTH: 35px; HEIGHT: 35px;"></div>
				</div>
				<!-- Navigator Skin End -->
				
			</div>
		</div>'.
		"<div id='master_full_screen' class='hint hint--left' data-hint='".__("Full Screen ", "ermak_miph")."'>
			<i class='fa fa-arrows-alt'></i>
		</div>";
		return $html;
	}
	function refresh_slide($slide_id, $is_empty=false, $inset_id=0)
	{
		global $Soling_Metagame_Constructor;
		$user_id				= get_current_user_id();
		$options				= Ermak_Booker_Plugin::get_instance()->options;
		$loc_id					= Ermak_Passport_Player::get_diclocation_id($user_id );
		switch($slide_id)
		{
			case EB_PERSONAL_MOBILE_PAGE:
				return 
				in_array($slide_id, $options['mobile_slides']) 
				? eb_get_slide_form(
					get_bloginfo('name'), 
					$is_empty 
					? 
					"<div class='eb_refresh'><i class='fa fa-refresh'></i></div>" 
					: 
					eb_get_personal(), 
					"url(" . SMC_URLPATH . "img/"  .$Soling_Metagame_Constructor->options['klapan_bckgrnd1'].")!important", 
					$slide_id, 
					$loc_id, 
					$inset_id
				) 
				: 
				"";
				return;
			case EB_PRODUCTS_MOBILE_PAGE:
				$disloc					= SMC_Location::get_child_location_ids($loc_id);
				$owners					= $Soling_Metagame_Constructor->all_user_locations($user_id);
				//insertLog("Ps.refresh_slide", $owners);
				if( count($owners)==0)  $owners = array(-1);
				$arr					= array("dislocation_id" => $disloc, "owner_id" => $owners);
				
				return 
				in_array($slide_id, $options['mobile_slides']) 
				? eb_get_slide_form(
					__("all Factories", "smp"), 
					$is_empty 
					? "<div class='eb_refresh'><i class='fa fa-refresh'></i></div>" 
					: 
					EB_Personal::get_factory_list($arr, true, "my_fctrs", $inset_id), 
					'#003322',
					$slide_id ,
					$loc_id , 
					$inset_id
				) 
				: 
				"";
				return;
			case EB_GOODS_BATCHS_MOBILE_PAGE:
				return 
				in_array($slide_id, $options['mobile_slides']) 
				?  eb_get_slide_form(
					__("all Goods", "smp") , 
					$is_empty 
					? "<div class='eb_refresh'><i class='fa fa-refresh'></i></div>" 
					: 
					EB_Personal::get_my_storage_form(array("dislocation_id"=> $loc_id ), true, "my_gbss", $inset_id), 
					'#111F1F', 
					$slide_id, 
					$loc_id, 
					$inset_id
				) 
				: 
				"";
				return;
			case EB_BANK_MOBILE_PAGE:
				return 
				in_array($slide_id, $options['mobile_slides']) 
				?  eb_get_slide_form(
					__("Bank", "smp") , 
					$is_empty 
					? "<div class='eb_refresh'><i class='fa fa-refresh'></i></div>" 
					: 
					"<div class='bb_container escroll'>".get_bank_page()."<div>", //EB_Personal::get_my_storage_form(array("dislocation_id"=> $loc_id ), true, "my_bank", $inset_id), 
					'#011A21', 
					$slide_id, 
					$loc_id, 
					$inset_id
				) 
				: 
				"";
				return;	
			case EB_ADMINISTRATOR_PAGE:
				return current_user_can("manage_options") 
				?  eb_get_slide_form(
					__("Administrator", "smp") , 
					$is_empty 
					? "<div class='eb_refresh'><i class='fa fa-refresh'></i></div>" 
					: 
					Admin_Passport_page::get_form(), 
					'#51171F',
					$slide_id, 
					$loc_id,  
					$inset_id
				) 
				: 
				"";
				return;
			case EB_STORE_MOBILE_PAGE:
				return in_array($slide_id, $options['mobile_slides']) 
				? eb_get_slide_form(
					__("Stores", "smp") ,
					$is_empty 
					? "<div class='eb_refresh'><i class='fa fa-refresh'></i></div>" 
					: EB_Personal::get_location_store_form( $loc_id, "store", $inset_id ), 
					'#662164', 
					$slide_id, 
					$loc_id, 
					$inset_id
				) 
				: 
				"";
				return;
			default: 
				list($title, $slide, $color, $exec, $args, $data)	= apply_filters("ermak_booker_mobile_slide", array(), $slide_id, $loc_id);
				return in_array($slide_id, $options['mobile_slides']) 
				?
					eb_get_slide_form(
					$title, 
					$is_empty ? "<div class='eb_refresh'><i class='fa fa-refresh'></i></div>": $slide, 
					$color, 
					$slide_id, 
					$loc_id,  
					$inset_id,
					$exec, 
					$args, 
					$data
					) 
				: 
					"";
		}
	}
	function eb_get_slide_form($title, $html, $color, $id, $loc_id, $inset_id=0, $exec='', $args='', $data='')
	{
		$location		= SMC_Location::get_instance($loc_id);
		$locname		= $location->name;
		return array("<div class='eb_slider_container' style='background:$color; height:100%' slide_id='$id' eb_exec='$exec', eb_args='$args'>
					<div u='thumb'>
						<div class='i' style='background-image:url(" . SMC_URLPATH . "/img/key_icon.png)!important;'></div>													
					</div>	
					<div>
					<table class='tbl'>
						<tr>
							<td class=''>					
								<div class='cell_0' style='height:100%;'>	
									<h3>$title <span style='font-weight: 700; color: rgb(170, 170, 170) !important;'>| $locname</span></h3>	<div class='eb_refresh'><i class='fa fa-refresh'></i></div>							
									$html
								</div>
							</td>
						</tr>
					</table>
					</div>
				</div>", $data);
	}
	function eb_get_personal()
	{
		$user			= wp_get_current_user();
		$html			= "<div id='eb_login_cont'>";
		if(!is_user_logged_in())	
		{	
			$html		.= "
			<div>
				<div class='description'>".
					__("Please register or log in", "smc"). 
			"	</div>
				<div class='button smc_padding smc-alert' target_name='login_form'>".
					__("Login", "smc").
			"	</div>
				<div class='button smc_padding smc-alert' target_name='register-form' params='min-height:200px;'>".
					__("Register", "smc").
			"	</div>".
			apply_filters("smc_login_form",
				'<div  id="login_form" style="display:none"> <!-- Login -->
					<div id="login-form">'.
						wp_login_form( array(
									'echo' => false,
									'redirect' => site_url( $_SERVER['REQUEST_URI'] ), 
									'form_id' => 'loginform',
									'label_username' => __( 'Username' ),
									'label_password' => __( 'Password' ),
									'label_remember' => __( 'Remember Me' ),
									'label_log_in' => __( 'Log In' ),
									'id_username' => 'user_login',
									'id_password' => 'user_pass',
									'id_remember' => 'rememberme',
									'id_submit' => 'wp-submit',
									'remember' => true,
									'value_username' => NULL,
									'value_remember' => false 
							) ).
					'</div>
				</div><!-- /Login -->').
			apply_filters("smc_register_form", Assistants::get_register_form()).
			"</div>";
		}
		else
		{
			$avatar		= get_avatar( get_current_user_id() , 70);
			$html		.= "
			
		<table BORDER='0' cellspacing='10' cellpadding='10' class='smc-login-table clear_table' style='vertical-align: top!important; text-align:left!important;'>
			<tr>
				<td>
					<div class='lp-widget-avatar'>												
						$avatar
					</div>
				</td>
			</tr>
			<tr>				
				<td valign='top' class='smc-login-table'>
					<div style='disply:inline-block; position:relative; width:100%;'>		
						<div style='display:block; margin-bottom:20px;'>
							<span class='lp-avatar-name'>".
								apply_filters("before_smc_logine_name", "",  false).
								$user->display_name.
								apply_filters("after_smc_logine_name", "",  true) . 
							"</span>
							<div>
								<div class='button smc_padding' id='logout'>" . __("Log out") . "</div>
							</div>
						</div>				
					</div>				
				</td>
			</tr>
		</table>";
		}
		return $html."</div>";
	}


										


?>


